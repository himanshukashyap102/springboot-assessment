package com.example.demo.Controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Model.Product;
import com.example.demo.Service.ProductService;

@RestController

public class ProductController {
	@Autowired
	ProductService service;
	
	@RequestMapping(value="/products", method=RequestMethod.POST)
	public Product createProduct(@RequestBody Product prod) {
	    return service.createProduct(prod);
	}
	
	@RequestMapping(value="/products", method=RequestMethod.GET)
	public List<Product> readProduct() {
	    return service.getProducts();
	}

	@RequestMapping(value="/products/{id}", method=RequestMethod.PUT)
	public Product readProducts(@PathVariable(value = "id") Long id, @RequestBody Product ProductDetails) {
	    return service.updateProduct(id, ProductDetails);
	}

	@RequestMapping(value="/products/{id}", method=RequestMethod.DELETE)
	public String deleteProduct(@PathVariable(value = "id") Long id) {
	    return service.deleteProduct(id);
	}
}
