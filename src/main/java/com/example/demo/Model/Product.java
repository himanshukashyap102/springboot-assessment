package com.example.demo.Model;

import javax.persistence.*;


@Entity
@Table(name="product")
public class Product {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long productId;
	@Column
	private String ProductName;
	@Column
	private String price;
	@Column
	private String Type;
	
	public Long getProductId() {
		return productId;
	}
	public String getProductName() {
		return ProductName;
	}
	public String getPrice() {
		return price;
	}
	public String getType() {
		return Type;
	}
	
	public void setProductId(Long productId) {
		this.productId = productId;
	}
	public void setProductName(String productName) {
		ProductName = productName;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public void setType(String type) {
		Type = type;
	}
	
	
}
