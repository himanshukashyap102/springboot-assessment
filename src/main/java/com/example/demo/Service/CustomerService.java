package com.example.demo.Service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.Model.Customer;
import com.example.demo.repository.CustomerRepository;
@Service
public class CustomerService {
	CustomerRepository customerrep;
	
	// CREATE 
	public Customer createEmployee(Customer cust) {
	    return customerrep.save(cust);
	}

	// READ
	public List<Customer> getEmployees() {
	    return customerrep.findAll();
	}

	// DELETE
	public void deleteEmployee(Long id) {
	    customerrep.deleteById(id);
	}
}
