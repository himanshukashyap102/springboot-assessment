package com.example.demo.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import com.example.demo.Model.Product;
import com.example.demo.repository.ProductRepository;

@Service
public class ProductService {
	@Autowired
	ProductRepository productrep;
	
	// CREATE 
	public Product createProduct(Product prod) {
	    
		return productrep.save(prod);
	}

	// READ
	public List<Product> getProducts() {
		return productrep.findAll();
	}

	// DELETE
	public String deleteProduct(Long id) {
	    if(!productrep.existsById(id)) throw new ProductNotFoundException();
		productrep.deleteById(id);
	    return "Product deleted successfully";
	}
	
	// UPDATE
	public Product updateProduct(Long id, Product ProductDetails) {
		if(!productrep.existsById(id)) throw new ProductNotFoundException();
			
			Product prod = productrep.findById(id).get();
	        prod.setProductName(ProductDetails.getProductName());
	        prod.setPrice(ProductDetails.getPrice());
	        prod.setType(ProductDetails.getType());
	        
	        return productrep.save(prod);                                
	}
}
